<?php


namespace Mcc\ApiSdkBundle\Interfaces;


use Doctrine\Common\Collections\ArrayCollection;

interface CrudInterface
{
    /**
     * @return string
     */
    public function getJwt(): string;

    /**
     * @param string $jwt
     * @return CrudInterface
     */
    public function setJwt(string $jwt): CrudInterface;


    /**
     * @param null $page
     * @param null $perPage
     * @return ArrayCollection
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCollection($page = null, $perPage = null): ArrayCollection;


    /**
     * @param $data
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function postCollection($data);


    /**
     * @param $id
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getItem($id);


    /**
     * @param $id
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deleteItem($id);

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function putItem($id, $data);
}