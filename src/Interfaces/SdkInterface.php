<?php


namespace Mcc\ApiSdkBundle\Interfaces;


use Symfony\Component\VarExporter\Exception\ClassNotFoundException;

interface SdkInterface
{
    public function setJWT(string $JWT);

    /**
     * @param string $type
     * @param string $uri
     * @param array $options
     * @return mixed
     * @throws ClassNotFoundException
     */
    public function get(string $type, string $uri, array $options = []);
}