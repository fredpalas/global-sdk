<?php


namespace Mcc\ApiSdkBundle\Services;


use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Serializer;
use Mcc\ApiSdkBundle\Interfaces\CrudInterface;
use Symfony\Component\VarExporter\Exception\ClassNotFoundException;

class CrudAdapter implements CrudInterface
{
    /** @var SdkClient */
    protected $sdkClient;
    /** @var string */
    protected $uri;
    /** @var string */
    protected $className;
    /** @var string */
    protected $createClassName;
    /** @var Serializer */
    protected $serializer;
    /** @var string */
    protected $jwt;
    /** @var int */
    protected $total = null;

    /**
     * CrudAdapter constructor.
     * @param SdkClient $sdkClient
     * @param Serializer $serializer
     * @param string $uri
     * @param string $classname
     * @param string $createClassName
     * @throws ClassNotFoundException
     */
    public function __construct(SdkClient $sdkClient, Serializer $serializer, string $uri, string $classname, string $createClassName)
    {
        $this->sdkClient = $sdkClient;
        $this->uri = $uri;
        $this->className = $classname;
        $this->serializer = $serializer;
        $this->createClassName = $createClassName;
        $this->checkClassName($classname);
        $this->checkClassName($createClassName);
    }

    /**
     * @return string
     */
    public function getJwt(): string
    {
        return $this->jwt;
    }

    /**
     * @param string $jwt
     * @return CrudInterface
     */
    public function setJwt(string $jwt): CrudInterface
    {
        $this->jwt = $jwt;
        $this->sdkClient->setJWT($jwt);

        return $this;
    }


    /**
     * @param null $page
     * @param null $perPage
     * @return ArrayCollection
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCollection($page = null, $perPage = null): ArrayCollection
    {
        $options = [
            'query' => [
                'page' => $page,
                'itemsPerPage' => $perPage,
            ],
        ];


        $data = $this->sdkClient->get($this->uri, 'array', $options);

        if (!isset($data['items'])) {
            throw new \Exception('item not found getCollection');
        }

        $this->total = isset($data['total']) ? $data['total'] : null;

        return $this->serializer->fromArray($data['items'], sprintf('ArrayCollection<%s>', $this->className));
    }


    /**
     * @param $data
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function postCollection($data)
    {
        if (!($data instanceof $this->createClassName)) {
            throw new \InvalidArgumentException(sprintf('The parameter $data need to be a instance of %s', $this->createClassName));
        }

        $dataJson = $this->serializer->toArray($data);

        return $this->sdkClient->post($this->uri, $dataJson, $this->className);
    }


    /**
     * @param $id
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getItem($id)
    {
        $uri = $this->uri.'/'.$id;

        return $this->sdkClient->get($uri, $this->className);
    }


    /**
     * @param $id
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deleteItem($id)
    {
        $uri = $this->uri.'/'.$id;

        return $this->sdkClient->delete($uri, 'boolean');
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function putItem($id, $data)
    {
        if (!($data instanceof $this->createClassName)) {
            throw new \InvalidArgumentException(sprintf('The parameter $data need to be a instance of %s', $this->createClassName));
        }
        $uri = $this->uri.'/'.$id;
        $dataJson = $this->serializer->toArray($data);

        return $this->sdkClient->put($uri, $dataJson, $this->className);
    }


    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param $className
     * @throws ClassNotFoundException
     */
    protected function checkClassName($className)
    {
        if (!class_exists($className)) {
            throw new ClassNotFoundException($className);
        }
    }
}
