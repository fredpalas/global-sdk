<?php

namespace Mcc\ApiSdkBundle\Services;

use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use JMS\Serializer\Serializer;
use Mcc\ApiSdkBundle\Interfaces\SdkInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class SdkClient implements SdkInterface
{
    /** @var ClientInterface */
    private $client;
    /** @var Serializer */
    private $serializer;

    private $headers = [];

    public function __construct(ClientInterface $client, Serializer $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    public function setJWT(string $token)
    {
        $this->headers = [
            'Authorization' => $token,
        ];
    }

    public function setApiKeyToken(string $apiKeyToken)
    {
        $this->headers = [
          'X-AUTH-TOKEN' => $apiKeyToken
        ];
    }

    public function clearAuth()
    {
        $this->headers = [];
    }

    /**
     * @param string $uri
     * @param string $type
     * @param array $options
     * @return mixed
     * @throws Exception
     * @throws GuzzleException
     */
    public function get(string $uri, string $type = 'array', array $options = [])
    {
        $options = $this->mergeOptions($options);

        return $this->callHttpClient('get', $uri, $options, $type);
    }

    /**
     * @param string $uri
     * @param array $data
     * @param string $type
     * @param array $options
     * @return mixed
     * @throws Exception
     * @throws GuzzleException
     */
    public function post(string $uri, array $data, string $type, array $options = [])
    {
        $options = $this->mergeOptions($options);

        $options['json'] = $data;

        return $this->callHttpClient('post', $uri, $options, $type);
    }

    /**
     * @param string $uri
     * @param string $type
     * @param array $options
     * @return mixed
     * @throws Exception
     * @throws GuzzleException
     */
    public function delete(string $uri, string $type = 'array', array $options = [])
    {
        $options = $this->mergeOptions($options);

        return $this->callHttpClient('delete', $uri, $options, $type);
    }

    /**
     * @param string $uri
     * @param array $data
     * @param string $type
     * @param array $options
     * @return mixed
     * @throws GuzzleException | Exception
     */
    public function put(string $uri, array $data, string $type, array $options = [])
    {
        $options = $this->mergeOptions($options);

        $options['json'] = $data;

        return $this->callHttpClient('put', $uri, $options, $type);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     * @param string $type
     * @return mixed
     * @throws GuzzleException | Exception
     */
    private function callHttpClient(string $method, string $uri, array $options, string $type)
    {
        $response = $this->client->request($method, $uri, $options);

        $this->checkStatusCode($response);

        return $this->serializeClass($type, $response);
    }

    private function mergeOptions($options)
    {
        return array_merge($options, ['headers' => $this->headers]);
    }

    /**
     * @param ResponseInterface $response
     * @throws Exception
     */
    private function checkStatusCode(ResponseInterface $response)
    {
        $dataString = (string)$response->getBody();
        $dataJson = json_decode($dataString, true);
        $message = $dataJson['message'] ?? null;
        switch ($code = $response->getStatusCode()) {
            case $code >= 200 && $code <= 208;
                break;
            case 401:
                throw new AuthenticationException($message);
                break;
            case 404:
                throw new NotFoundHttpException($message);
                break;
            case 403:
                throw new HttpException(403, $message);
            case 422:
                throw new UnprocessableEntityHttpException($message);
            case 500:
            default;
                throw new Exception($message);
                break;
        }
    }

    /**
     * @param $type
     * @param ResponseInterface $response
     * @return mixed
     */
    private function serializeClass($type, ResponseInterface $response)
    {
        $dataString = (string)$response->getBody();

        return $this->serializer->deserialize($dataString, $type, 'json');
    }
}