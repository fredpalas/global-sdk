<?php


namespace Mcc\ApiSdkBundle\DependencyInjection;


use Mcc\ApiSdkBundle\Services\SdkClient;
use Mcc\ApiSdkBundle\Services\CrudAdapter;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class ApiSdkExtension extends Extension
{


    /**
     * @inheritDoc
     * @param array $configs
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
//        // TODO: Implement load() method.
//        $loader = new XmlFileLoader($container, new FileLocator(dirname(__DIR__).'/Resources/config'));
//        $loader->load('services.xml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        foreach ($config['crud_clients'] as $name => $options) {

            $definition = new Definition(
                SdkClient::class, [
                    new Reference($options['http_client']),
                    new Reference('jms_serializer.serializer'),
                ]
            );
            $definition->setLazy($options['lazy']);
            // set service name based on client name
            $serviceName = sprintf('%s.sdk_client.%s', $this->getAlias(), $name);
            $container->setDefinition($serviceName, $definition);

            $crudDefinition = new Definition(
                $options['crud_class'],
                [
                    new Reference($serviceName),
                    new Reference('jms_serializer.serializer'),
                    $options['uri'],
                    $options['response_class'],
                    $options['write_class'],
                ]
            );
            $serviceCrudName = sprintf('%s.crud_client.%s', $this->getAlias(), $name);
            $container->setDefinition($serviceCrudName, $crudDefinition);
        }
    }


}