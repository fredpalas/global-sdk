<?php


namespace Mcc\ApiSdkBundle\DependencyInjection;


use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Mcc\ApiSdkBundle\Interfaces\CrudInterface;
use Mcc\ApiSdkBundle\Services\CrudAdapter;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{



    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder('api_sdk');

        $root = (method_exists($builder, 'getRootNode')) ? $builder->getRootNode() : $builder->root('api_sdk');

        $root
            ->children()
                ->append($this->createCrudNode())
                ->end()
            ->end();

        return $builder;
    }

    private function createCrudNode(): ArrayNodeDefinition
    {
        $builder = new TreeBuilder('crud_clients');
        /** @var ArrayNodeDefinition $node */
        $node = (method_exists($builder, 'getRootNode')) ? $builder->getRootNode() : $builder->root('crud_clients');
        /** @var NodeBuilder $nodeChildren */
        $nodeChildren = $node->useAttributeAsKey('name')->prototype('array')->children();

        $nodeChildren
            ->scalarNode('http_client')
                ->isRequired()
                ->cannotBeEmpty()
                ->validate()
                    ->ifTrue(
                        function ($v) {
                            if (!class_exists($v)) {
                                return !is_string($v);
                            }

                            return !in_array(ClientInterface::class, class_implements($v));
                        }
                    )
                    ->thenInvalid(sprintf('http_client need to implement %s', Client::class))
                ->end()
            ->end()
            ->scalarNode('uri')
                ->defaultValue(null)
                ->validate()
                    ->ifTrue(
                        function ($v) {
                            return !is_string($v);
                        }
                    )
                    ->thenInvalid('base_url can be: string')
                ->end()
            ->end()
            ->scalarNode('response_class')
                ->defaultValue(null)
                ->isRequired()
                ->cannotBeEmpty()
                ->validate()
                    ->ifTrue(function ($v) {
                        return !class_exists($v);
                    })
                    ->thenInvalid('response_class need to be a valid Class')
                ->end()
            ->end()
            ->scalarNode('write_class')
                ->isRequired()
                ->cannotBeEmpty()
                ->defaultValue(null)
                    ->validate()
                    ->ifTrue(function ($v) {
                        return !class_exists($v);
                    })
                    ->thenInvalid('response_class need to be a valid Class')
                ->end()
            ->end()
            ->scalarNode('crud_class')
                ->defaultValue(CrudAdapter::class)
                    ->validate()
                    ->ifTrue(function ($v){
                        if (!class_exists($v)) {
                            return !is_string($v);
                        }

                        return !in_array(CrudInterface::class, class_implements($v));
                    })
                    ->thenInvalid(sprintf('crud_class need to implement %s', CrudInterface::class))
                ->end()
            ->end()
            ->booleanNode('lazy')->defaultValue(true)->end();

        return $node;
    }
}